/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.namnt.hellospringweb;

import java.util.List;

/**
 *
 * @author Nguyen The Nam
 */
public class CustomersWrapper {

    private int total;
    private List<Customers> data;

    public CustomersWrapper() {
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Customers> getData() {
        return data;
    }

    public void setData(List<Customers> data) {
        this.data = data;
    }

}
