-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: hello_spring
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Bennie Parenteau',30,'2017-01-22 12:00:01'),(2,'Veta Nordby',23,'2017-01-22 12:00:01'),(3,'Asley Ehlers',32,'2017-01-22 12:00:01'),(4,'Susanna Easler',42,'2017-01-22 12:00:01'),(5,'Kym Wendling',5,'2017-01-22 12:00:01'),(6,'Rob Garraway',2,'2017-01-22 12:00:01'),(7,'Drucilla Huber',12,'2017-01-22 12:00:01'),(8,'Leonida Coutu',66,'2017-01-22 12:00:01'),(9,'Amada Dycus',34,'2017-01-22 12:00:01'),(10,'Racquel Jeter',23,'2017-01-22 12:00:01'),(11,'Ozella Mickles',64,'2017-01-22 12:00:01'),(12,'Mitzi Mcfall',34,'2017-01-22 12:00:01'),(13,'Gale Hewitt',65,'2017-01-22 12:00:01'),(14,'Angie Flinn',32,'2017-01-22 12:00:01'),(15,'Shantae Peet',21,'2017-01-22 12:00:01'),(16,'Renetta Greenland',26,'2017-01-22 12:00:01'),(17,'Malissa Custard',14,'2017-01-22 12:00:01'),(18,'Jennifer Myerson',56,'2017-01-22 12:00:01'),(19,'Hilario Anderton',53,'2017-01-22 12:00:01'),(20,'Rusty Westrick',72,'2017-01-22 12:00:01'),(21,'Dawne Barnes',11,'2017-01-22 12:00:01'),(22,'Ophelia Arndt',23,'2017-01-22 12:00:01'),(23,'Jeanene Casavant',27,'2017-01-22 12:00:01'),(24,'Lilliana Collard',24,'2017-01-22 12:00:01'),(25,'Sarai Merlin',29,'2017-01-22 12:00:01'),(26,'Contessa Barth',46,'2017-01-22 12:00:01'),(27,'Judie Moone',21,'2017-01-22 12:00:01'),(28,'Leland Cavanagh',42,'2017-01-22 12:00:01'),(29,'Marcellus Munk',42,'2017-01-22 12:00:01'),(30,'Crysta Hobbs',57,'2017-01-22 12:00:01');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-11 14:24:02
